/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Slot,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:unchecked`,
})
export class UncheckedCondition extends ConditionBlock {
    @condition
    allUnchecked(): boolean {
        const slots = this.slots;

        return (
            !slots ||
            !slots.each((slot: Slot) => {
                const checkbox = this.valueOf<boolean>(slot);

                return (checkbox && checkbox.value) || false;
            })
        );
    }
}
