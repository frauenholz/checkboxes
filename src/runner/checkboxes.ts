/** Dependencies */
import {
    NodeBlock,
    Slots,
    Str,
    Value,
    each,
    filter,
    findFirst,
    isNumberFinite,
    map,
    reduce,
    validator,
} from "tripetto-runner-foundation";
import { ICheckbox } from "./checkbox";

export abstract class Checkboxes extends NodeBlock<{
    readonly checkboxes?: ICheckbox[];
    readonly min?: number;
    readonly max?: number;
    readonly required?: boolean;
    readonly randomize?: boolean;
    readonly formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";
    formatSeparatorCustom?: string;
}> {
    /** Contains the randomized checkboxes order. */
    private randomized?: {
        readonly index: number;
        readonly id: string;
    }[];

    /** Contains the counter slot. */
    readonly counterSlot = this.valueOf<number, Slots.Number>(
        "counter",
        "feature"
    );

    /** Contains the concatenation slot. */
    readonly concatenationSlot = this.valueOf<string, Slots.Text>(
        "concatenation",
        "feature"
    );

    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Retrieves if a checked checkbox is required. */
    get required(): boolean {
        return this.props.required || false;
    }

    private transform(): void {
        const checkboxes = filter(
            map(this.props.checkboxes, (checkbox) => ({
                id: checkbox.id,
                label: checkbox.value || checkbox.name || "",
                exclusive: checkbox.exclusive,
                valueRef: this.valueOf<boolean>(checkbox.id),
            })),
            (checkbox) => checkbox.valueRef?.value === true
        ).sort((a, b) => (b.valueRef?.time || 0) - (a.valueRef?.time || 0));
        const lastChecked = checkboxes.length && checkboxes[0];

        if (lastChecked) {
            checkboxes.forEach((c) => {
                if (
                    c.id !== lastChecked.id &&
                    (lastChecked.exclusive || c.exclusive)
                ) {
                    c.valueRef!.value = false;
                }
            });
        }

        if (this.props.max) {
            const max = this.props.max;
            let n = 0;

            checkboxes.forEach((c) => {
                if (c.valueRef?.value === true) {
                    n++;

                    if (n > max) {
                        c.valueRef!.value = false;
                    }
                }
            });
        }

        if (this.counterSlot) {
            const n = filter(
                checkboxes,
                (checkbox) => checkbox.valueRef?.value === true
            ).length;

            this.counterSlot.set(n);
        }

        if (this.concatenationSlot) {
            const list: string[] = [];
            let s = "";
            let n = 0;

            each(this.props.checkboxes, (checkbox) => {
                const label = checkbox.value || checkbox.name || "";

                if (
                    label &&
                    this.valueOf<boolean>(checkbox.id)?.value === true
                ) {
                    switch (this.props.formatSeparator) {
                        case "space":
                            s += (s === "" ? "" : " ") + label;
                            break;
                        case "list":
                            s += (s === "" ? "" : "\n") + label;
                            break;
                        case "bullets":
                            s += (s === "" ? "" : "\n") + "- " + label;
                            break;
                        case "numbers":
                            s += (s === "" ? "" : "\n") + `${++n}. ${label}`;
                            break;
                        case "conjunction":
                        case "disjunction":
                            list.push(label);
                            break;
                        case "custom":
                            s +=
                                (s === ""
                                    ? ""
                                    : this.props.formatSeparatorCustom || "") +
                                label;
                            break;
                        default:
                            s += (s === "" ? "" : ", ") + label;
                            break;
                    }
                }
            });

            if (
                this.props.formatSeparator === "conjunction" ||
                this.props.formatSeparator === "disjunction"
            ) {
                try {
                    const formatter = new Intl.ListFormat(
                        this.context.l10n.current || "en",
                        { type: this.props.formatSeparator }
                    );

                    s = formatter.format(list);
                } catch {
                    s = Str.iterateToString(list, ", ");
                }
            }

            this.concatenationSlot.set(s);
        }
    }

    private score(checkbox: ICheckbox): void {
        if (this.scoreSlot && isNumberFinite(checkbox.score)) {
            this.scoreSlot.set(
                reduce(
                    this.props.checkboxes,
                    (score, c) =>
                        score +
                        ((this.valueOf(c.id)?.value === true && c.score) || 0),
                    0
                )
            );
        }
    }

    /** Retrieves a checkbox slot. */
    checkboxSlot(
        checkbox: ICheckbox
    ): Value<boolean, Slots.Boolean> | undefined {
        return this.valueOf<boolean>(checkbox.id, "dynamic", {
            confirm: true,
            onChange: () => {
                this.transform();
                this.score(checkbox);
            },
            onContext: (value, context) => {
                if (this.scoreSlot) {
                    context
                        .contextualValueOf(this.scoreSlot)
                        ?.set(
                            value.value === true
                                ? checkbox.score || 0
                                : undefined
                        );
                }
            },
        });
    }

    /** Retrieves if a checkbox is checked. */
    isChecked(checkbox: ICheckbox): boolean {
        const checkboxSlot = this.checkboxSlot(checkbox);

        return checkboxSlot?.value || false;
    }

    /** Sets a checkbox state. */
    check(checkbox: ICheckbox, checked: boolean): boolean {
        const checkboxSlot = this.checkboxSlot(checkbox);

        if (checkboxSlot) {
            checkboxSlot.value = checked;

            return checkboxSlot.value;
        }

        return false;
    }

    /** Toggles a checkbox. */
    toggle(checkbox: ICheckbox): void {
        const checkboxSlot = this.checkboxSlot(checkbox);

        if (checkboxSlot) {
            checkboxSlot.value = !checkboxSlot.value;
        }
    }

    /** Retrieves the checkboxes. */
    checkboxes<T>(props: {
        readonly tabIndex?: number;
        readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => T;
    }): (Omit<ICheckbox, "value" | "description"> & {
        readonly label: T;
        readonly description?: T;
        readonly value?: Value<boolean, Slots.Boolean>;
        readonly tabIndex?: number;
    })[] {
        const n =
            (this.props.max &&
                filter(
                    this.props.checkboxes,
                    (checkbox) => this.checkboxSlot(checkbox)?.value === true
                ).length) ||
            0;
        const checkboxes =
            this.props.checkboxes?.map((checkbox) => {
                const value = this.checkboxSlot(checkbox);

                return {
                    ...checkbox,
                    label: props.markdownifyToJSX(checkbox.name, false),
                    description:
                        (checkbox.description &&
                            props.markdownifyToJSX(
                                checkbox.description,
                                true
                            )) ||
                        undefined,
                    value,
                    tabIndex: props.tabIndex,
                    disabled:
                        (this.props.max &&
                            n >= this.props.max &&
                            value?.value !== true) ||
                        false,
                };
            }) || [];

        if (this.props.randomize && checkboxes.length > 1) {
            if (
                !this.randomized ||
                this.randomized.length !== checkboxes.length ||
                findFirst(
                    this.randomized,
                    (checkbox) => checkboxes[checkbox.index]?.id !== checkbox.id
                )
            ) {
                this.randomized = checkboxes.map((checkbox, index) => ({
                    index,
                    id: checkbox.id,
                }));

                let length = this.randomized.length;

                while (--length) {
                    const index = Math.floor(Math.random() * length);
                    const temp = this.randomized[length];

                    this.randomized[length] = this.randomized[index];
                    this.randomized[index] = temp;
                }
            }

            return this.randomized.map(
                (checkbox) => checkboxes[checkbox.index]
            );
        } else if (this.randomized) {
            this.randomized = undefined;
        }

        return checkboxes;
    }

    @validator
    validate(): boolean {
        if (this.props.min || this.props.max) {
            const n = filter(
                this.props.checkboxes,
                (checkbox) => this.checkboxSlot(checkbox)?.value === true
            ).length;

            if (
                (this.props.min && n < this.props.min) ||
                (this.props.max && n > this.props.max)
            ) {
                return false;
            }
        }

        if (this.props.required) {
            return findFirst(
                this.props.checkboxes,
                (checkbox: ICheckbox) =>
                    this.valueOf<boolean>(checkbox.id)?.value === true
            )
                ? true
                : false;
        }

        return true;
    }
}

// As long as the typings for Intl are incomplete, we need to declare them ourselves.
// See https://github.com/microsoft/TypeScript/issues/46907
declare namespace Intl {
    class ListFormat {
        constructor(
            locales?: string | string[],
            options?: {
                type?: "conjunction" | "disjunction";
            }
        );
        format(values: string[]): string;
    }
}
