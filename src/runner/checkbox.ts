export interface ICheckbox {
    /** Id of the checkbox. */
    readonly id: string;

    /** Name of the checkbox. */
    readonly name: string;

    /** Description for the checkbox. */
    readonly description?: string;

    /** Value of the checkbox. */
    readonly value?: string;

    /** Score of the checkbox. */
    readonly score?: number;

    /** Specifies if the checkbox is an exclusive value. */
    readonly exclusive?: boolean;
}
