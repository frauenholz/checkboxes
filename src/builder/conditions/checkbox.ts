/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    collection,
    definition,
    editor,
    markdownifyToString,
    pgettext,
    tripetto,
} from "tripetto";
import { Checkbox } from "../checkbox";

/** Assets */
import ICON from "../../../assets/condition.svg";
import ICON_CHECKED from "../../../assets/checked.svg";
import ICON_UNCHECKED from "../../../assets/unchecked.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:checkboxes", "Checkbox state");
    },
})
export class CheckboxCondition extends ConditionBlock {
    @affects("#name")
    @collection("#checkboxes")
    checkbox: Checkbox | undefined;

    @affects("#icon")
    @definition
    checked = true;

    get icon() {
        return this.checked ? ICON_CHECKED : ICON_UNCHECKED;
    }

    get name() {
        return (
            markdownifyToString(this.checkbox?.name || "") || this.type.label
        );
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            controls: [
                new Forms.Checkbox(
                    markdownifyToString(this.checkbox?.name || "") ||
                        pgettext("block:checkboxes", "Checkbox is checked"),
                    Forms.Checkbox.bind(this, "checked", true)
                ),
            ],
        });
    }
}
