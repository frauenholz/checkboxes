/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { TCounterModes } from "../../runner/conditions/counter";

/** Assets */
import ICON from "../../../assets/counter.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:counter`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:checkboxes", "Verify counter");
    },
})
export class CounterCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TCounterModes = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Number) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:checkboxes",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "not-equal":
                    return `@${slot.id} \u2260 ${value}`;
                case "above":
                case "below":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.slot?.label || this.node?.label;
    }

    private parse(
        slot: Slots.Number,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return slot.toString(value, (n) => L10n.locale.number(n, 0, false));
        } else if (
            isString(value) &&
            value &&
            lookupVariable(this, value)?.label
        ) {
            return `@${value}`;
        }

        return "\\_\\_";
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:checkboxes", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TCounterModes>(
                    [
                        {
                            label: pgettext(
                                "block:checkboxes",
                                "Counter is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:checkboxes",
                                "Counter is not equal to"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext(
                                "block:checkboxes",
                                "Counter is lower than"
                            ),
                            value: "below",
                        },
                        {
                            label: pgettext(
                                "block:checkboxes",
                                "Counter is higher than"
                            ),
                            value: "above",
                        },
                        {
                            label: pgettext(
                                "block:checkboxes",
                                "Counter is between"
                            ),
                            value: "between",
                        },
                        {
                            label: pgettext(
                                "block:checkboxes",
                                "Counter is not between"
                            ),
                            value: "not-between",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TCounterModes>) => {
                    to.visible(
                        mode.value === "between" || mode.value === "not-between"
                    );

                    switch (mode.value) {
                        case "equal":
                            from.title = pgettext(
                                "block:checkboxes",
                                "If counter equals"
                            );
                            break;
                        case "not-equal":
                            from.title = pgettext(
                                "block:checkboxes",
                                "If counter not equals"
                            );
                            break;
                        case "below":
                            from.title = pgettext(
                                "block:checkboxes",
                                "If counter is lower than"
                            );
                            break;
                        case "above":
                            from.title = pgettext(
                                "block:checkboxes",
                                "If counter is higher than"
                            );
                            break;
                        case "between":
                            from.title = pgettext(
                                "block:checkboxes",
                                "If counter is between"
                            );
                            break;
                        case "not-between":
                            from.title = pgettext(
                                "block:checkboxes",
                                "If counter is not between"
                            );
                            break;
                    }
                }),
            ],
        });

        const addCondition = (property: "value" | "to", title: string) => {
            const value = this[property];
            const numberControl = new Forms.Numeric(
                isNumberFinite(value) ? value : 0
            )
                .label(pgettext("block:checkboxes", "Use fixed number"))
                .autoFocus(property === "value")
                .escape(this.editor.close)
                .enter(
                    () =>
                        ((this.mode !== "between" &&
                            this.mode !== "not-between") ||
                            property === "to") &&
                        this.editor.close()
                )
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variables = populateVariables(
                this,
                (slot) =>
                    slot instanceof Slots.Number ||
                    slot instanceof Slots.Numeric,
                isString(value) ? value : undefined,
                true,
                this.slot?.id
            );
            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:checkboxes", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor.form({
                title,
                controls: [
                    new Forms.Radiobutton<"number" | "variable">(
                        [
                            {
                                label: pgettext("block:checkboxes", "Number"),
                                value: "number",
                            },
                            {
                                label: pgettext("block:checkboxes", "Value"),
                                value: "variable",
                                disabled: variables.length === 0,
                            },
                        ],
                        isString(value) ? "variable" : "number"
                    ).on((type) => {
                        numberControl.visible(type.value === "number");
                        variableControl.visible(type.value === "variable");

                        if (numberControl.isObservable) {
                            numberControl.focus();
                        }
                    }),
                    numberControl,
                    variableControl,
                ],
            });
        };

        const from = addCondition(
            "value",
            pgettext("block:checkboxes", "If counter equals")
        );
        const to = addCondition("to", pgettext("block:checkboxes", "And"));
    }
}
