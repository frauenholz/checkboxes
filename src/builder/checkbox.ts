/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    Slots,
    affects,
    alias,
    created,
    definition,
    deleted,
    editor,
    insertVariable,
    isBoolean,
    isString,
    markdownifyToString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
    score,
} from "tripetto";
import { Checkboxes } from "./index";
import { ICheckbox } from "../runner";

export class Checkbox extends Collection.Item<Checkboxes> implements ICheckbox {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    description?: string;

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForFalse?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @definition("boolean", "optional")
    @affects("#name")
    exclusive?: boolean;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        const checkboxName =
            (this.name &&
                markdownifyToString(
                    this.name,
                    Markdown.MarkdownFeatures.Formatting |
                        Markdown.MarkdownFeatures.Hyperlinks
                )) ||
            undefined;

        const slot = this.ref.slots.dynamic({
            type: Slots.Boolean,
            reference: this.id,
            label: pgettext("block:checkboxes", "Checkbox"),
            sequence: this.index,
            name: checkboxName,
            alias: this.value,
            required: this.ref.required,
            exportable:
                this.ref.format !== "concatenate" && this.ref.exportable,
            pipeable: {
                label: pgettext("block:checkboxes", "Checkbox"),
                content:
                    this.name !== checkboxName
                        ? {
                              string: checkboxName || "",
                              markdown: this.name,
                          }
                        : "name",
                alias: this.ref.alias,
                legacy: "Checkbox",
            },
        });

        slot.labelForTrue =
            this.labelForTrue ||
            this.ref.labelForTrue ||
            pgettext("block:checkboxes", "Checked");

        slot.labelForFalse =
            this.labelForFalse ||
            this.ref.labelForFalse ||
            pgettext("block:checkboxes", "Not checked");
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:checkboxes", "Name"),
            form: {
                title: pgettext("block:checkboxes", "Checkbox name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:checkboxes", "Description"),
            form: {
                title: pgettext("block:checkboxes", "Description"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "description", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:checkboxes", "Options"));

        this.editor.option({
            name: pgettext("block:checkboxes", "Exclusivity"),
            form: {
                title: pgettext("block:checkboxes", "Exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:checkboxes",
                            "Uncheck all other checkboxes when checked"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.exclusive),
        });

        const defaultLabelForTrue = pgettext("block:checkboxes", "Checked");
        const defaultLabelForFalse = pgettext(
            "block:checkboxes",
            "Not checked"
        );

        this.editor.option({
            name: pgettext("block:checkboxes", "Labels"),
            form: {
                title: pgettext("block:checkboxes", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:checkboxes",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
        });

        this.editor.option({
            name: pgettext("block:checkboxes", "Identifier"),
            form: {
                title: pgettext("block:checkboxes", "Identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:checkboxes",
                            "If a checkbox identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:checkboxes", "Score"),
            form: {
                title: pgettext("block:checkboxes", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}
